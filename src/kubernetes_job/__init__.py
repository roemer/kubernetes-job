from .job_manager import JobManager, current_job, is_failed, is_succeeded, is_completed, is_active, job_name, job_status

from .__version__ import __version__
