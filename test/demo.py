import logging
import os
import tempfile
import time
import yaml
import sys
from pathlib import Path
from kubernetes import client, config

from kubernetes_job import JobManager, is_completed
from funcs import add, calc_pi

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)

# retrieve cluster details from environment variables
host_url = os.environ.get("HOST_URL")
cacert = os.environ.get("CACERT")
token = os.environ.get("TOKEN")

configuration = None

if host_url:
    # initialize configuration for token authentication
    # this is the way to go if we're using a service account
    configuration = client.Configuration()
    configuration.api_key["authorization"] = token
    configuration.api_key_prefix['authorization'] = 'Bearer'
    configuration.host = host_url

    # configuration.ssl_ca_cert expects a file containing the certificates,
    # so we generate a temporary file to hold those
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as tf:
        tf.write(cacert)
        configuration.ssl_ca_cert = tf.name

else:
    # try to initialize from $HOME/.kube/config (eg. kubectl config file)
    config.load_kube_config()

# initialize the Kubernetes client
k8s_client = client.ApiClient(configuration=configuration)

# Path to worker configuration
yaml_path = Path(__file__).parent / 'job.yml'

# we're loading the yaml file here;
# we could also supply the path when initializing the JobManager
with Path(yaml_path).open() as f:
    yaml_spec = yaml.safe_load(f)

# initialize the job manager
manager = JobManager(k8s_client=k8s_client, k8s_job_spec=yaml_spec)

# create a new job
new_job = manager.create_job(calc_pi, 100, 1)
logging.info(f"Created job {new_job.metadata.name}")

# list all jobs
for job in manager.list_jobs():
    logging.info(f"Found: {job.metadata.name}")

# get the status of a job
job_status = manager.read_job(new_job)
while not is_completed(job_status):
    logging.info(f"Status: {job_status.status}")
    job_status = manager.read_job(new_job)
    time.sleep(5)

# clean up jobs
manager.clean_jobs()

# delete a job
# manager.delete_job(new_job)

