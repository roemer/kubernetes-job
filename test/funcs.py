import logging
import time

logger = logging.getLogger(__name__)


def add(a, b, **kwargs):
    logger.info(f"Add invoked with arguments a:{a}, b:{b}, kwargs:{kwargs}")
    result = a + b

    logger.info(f"The result is: {result}")
    return result


def make_pi(n):
    q, r, t, k, m, x = 1, 0, 1, 1, 3, 3
    for j in range(n):
        if 4 * q + r - t < m * t:
            yield m
            q, r, t, k, m, x = 10*q, 10*(r-m*t), t, k, (10*(3*q+r))//t - 10*m, x
        else:
            q, r, t, k, m, x = q*k, (2*q+r)*x, t*x, k+1, (q*(7*k+2)+r*x)//(t*x), x+2


def calc_pi(n_digits, sleep=0):
    digits = []

    for digit in make_pi(n_digits):
        digits.append(str(digit))
        logger.info("%s.%s" % (digits[0], "".join(digits[1:])))
        time.sleep(sleep)


def no_args_no_kwargs():
    return no_args_no_kwargs.__name__


def kwargs_only(a=1, b=2, **kwargs):
    logger.info(f"{a}; {b}; {kwargs}")
    return kwargs_only.__name__


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    calc_pi(100, 1)