API reference
=========================================

===============
JobManager
===============

..  autoclass:: kubernetes_job.JobManager
    :members:


===============
Helpers
===============

..  autofunction:: kubernetes_job.job_name
..  autofunction:: kubernetes_job.job_status
..  autofunction:: kubernetes_job.is_completed
..  autofunction:: kubernetes_job.is_succeeded
..  autofunction:: kubernetes_job.is_failed
..  autofunction:: kubernetes_job.is_active

..  autodata:: kubernetes_job.current_job
    :no-value:
    :annotation: =kubernetes_job.job_func_def.JobFuncDef

    Current `JobFuncDef` when executing a Kubernetes-job (as runner), otherwise `None`.

==============
job_func_def
==============

`job_func_def` contains helper classes for the serialization and execution of the function call.

..  automodule:: kubernetes_job.job_func_def
    :members:
