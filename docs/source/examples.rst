Examples
=========================================

===============
demo.py
===============

A small demo application illustrating the use of the Kubernetes-job API.

.. literalinclude:: ../../test/demo.py
   :language: python
   :linenos:


===============
job.yaml
===============

An example of a job spec template.

.. literalinclude:: ../../test/job.yml
   :language: yaml
   :linenos:


====================
service_account.yaml
====================

An example of the Kubernetes configuration needed to create a service account for job management.

.. literalinclude:: ../../test/service_account.yml
   :language: yaml
   :linenos:

To execute, run the following command:

.. code-block:: bash

    kubectl apply -f service_account.yml

