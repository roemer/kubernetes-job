# Links  

## Project links

**Project homepage:** [https://gitlab.com/roemer/kubernetes-job](https://gitlab.com/roemer/kubernetes-job)

**Documentation:** [https://kubernetes-job.readthedocs.io](https://kubernetes-job.readthedocs.io)

**Pypi**: [https://pypi.org/project/kubernetes-job](https://pypi.org/project/kubernetes-job)


## Kubernetes

**Kubernetes Python client:** [https://github.com/kubernetes-client/python](https://github.com/kubernetes-client/python)