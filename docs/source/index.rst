.. Kubernetes-job documentation master file, created by
   sphinx-quickstart on Tue Mar 16 23:53:56 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Kubernetes-job documentation!
============================================

Kubernetes-job is a Python library for starting Kubernetes batch jobs,
in the simplest way possible:

.. code-block:: python

   from kubernetes_job import JobManager

   manager = JobManager(kubernetes_client, 'job.yaml')
   job = manager.create_job(my_job_function, arg_1, arg_2)




**Project homepage:** https://gitlab.com/roemer/kubernetes-job

**Documentation:** https://kubernetes-job.readthedocs.io

**Pypi**: https://pypi.org/project/kubernetes-job

**Version:** |version|


.. toctree::
   :maxdepth: 2
   :caption: Contents

   Introduction <README.md>
   kubernetes
   api
   examples
   links
   license

* :ref:`genindex`
