IMAGE_NAME = kubernetes-job
DOCKER_BUILD_ARGS=

HOST_URL = `jq -r .HOST_URL env.json`
CACERT = `jq -r .CACERT env.json`
TOKEN = `jq -r .TOKEN env.json`
KUBERNETES_JOB_FUNC = `jq -r .KUBERNETES_JOB_FUNC env.json`

.PHONY: clean
clean:
	rm -rf dist/
	rm -rf build/
	rm -rf src/kubernetes_job.egg-info/
	rm -rf docker-dist/

build:
	python -m build


dist: build
	# copy entire source
	mkdir -p docker-dist/app

	cp -r test/* docker-dist/app
	cp dist/*.tar.gz docker-dist/app/package.tar.gz

	$(MAKE) docker-build

docker-build:
	docker build -t $(IMAGE_NAME) $(DOCKER_BUILD_ARGS) -f Dockerfile ./docker-dist

docker-it:
	docker run -e HOST_URL="$(HOST_URL)" -e CACERT="$(CACERT)" -e TOKEN="$(TOKEN)" --network=host -it $(IMAGE_NAME) /bin/bash

docker-run:
	docker run -e KUBERNETES_JOB_FUNC="$(KUBERNETES_JOB_FUNC)" $(IMAGE_NAME) kubernetes-job

docker-test:
	docker run $(IMAGE_NAME) pytest

docker-save:
	docker save $(IMAGE_NAME) > image.tar

docker-load:
	docker load < image.tar

.PHONY: test
test:
	$(MAKE) test-internal -C src --makefile ../makefile

test-internal:
	python -m pytest ../test/ --junit-xml=test-report.xml

upload:
	twine upload dist/*

.PHONY: doc
doc:
	$(MAKE) clean html -C docs

all: clean dist test doc