FROM python:3.6-slim

RUN apt-get update && \
    apt-get install curl nano gnupg2 -y && \
    #
    # Google Cloud SDK, kubectl
    #
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && \
    apt-get update && \
    apt-get install google-cloud-sdk kubectl -y --no-install-recommends && \
    #
    # clean up apt
    #
    apt-get autoremove --purge -y && apt-get clean && rm -rf /var/lib/apt/lists/* && \
    #
    # pip, pytest
    mkdir app && pip install --upgrade pip && pip install pytest

COPY . .

WORKDIR app

RUN pip install package.tar.gz

ENV PYTHONPATH=/app

CMD ["python", "demo.py"]